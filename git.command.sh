#!/bin/sh

echo "Add files and do a local commit"
git add .

echo "Checking status"
git status

git commit -am "added provider, terraform, resource block"

echo "Pushing to gitlab repository"
git push