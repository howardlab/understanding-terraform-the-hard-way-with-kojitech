## PROVIDER BLOCK

provider "aws" {
    region = "us-east-1"
    profile = "default"
}


## TERRAFORM BLOCK
terraform {
    required_version = ">=1.1.0"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"    ## AWS API version
    }
  }
}


## RESOURCE BLOCK
resource "aws_vpc" "main" {
  cidr_block       = var.vpc_cidr   # CIDR (re-usable)
  instance_tenancy = "default"

  tags = {
    Name = var.ec2_tag_name
  }
}

resource "aws_instance" "ec2_instance" {
  ami           = var.ami_id  # us-esast-1
  instance_type = var.ec2_instance

  tags = {
    name = var.ec2_tag_name
  }

}

resource "aws_subnet" "private1" {
  vpc_id     = local.local_vpc_id   #resource_name.local_name.attributeclear
  cidr_block = var.subnet_cidr[0]
  ## parameters ---> from doc -- Reference Argument

  ### availability_zone = "us-east-1a"    #Data source
  
  availability_zone = data.aws_availability_zones.available.names[0] #updated to accomodate data sources
}

resource "aws_subnet" "private2" {
  vpc_id     = local.local_vpc_id  #resource_name.local_name.attributeclear
  cidr_block = var.subnet_cidr[1]
  ## parameters ---> from doc -- Reference Argument
  availability_zone = data.aws_availability_zones.available.names[1]

}

resource "aws_subnet" "private3" {
  vpc_id     = local.local_vpc_id   #resource_name.local_name.attributeclear
  cidr_block = var.subnet_cidr[2]
  ## parameters ---> from doc -- Reference Argument
  availability_zone = data.aws_availability_zones.available.names[1]

}

resource "aws_subnet" "private4" {
  vpc_id     = local.local_vpc_id   #resource_name.local_name.attributeclear
  cidr_block = var.subnet_cidr[3]
  ## parameters ---> from doc -- Reference Argument
  availability_zone = data.aws_availability_zones.available.names[0]

}


## DATA SOURCE BLOCK
## USed to pull down resources from the relevant provider - 
data "aws_availability_zones" "available" {
  state = "available"
}
##data.aws_availability_zones.available.names[0] = us-east-1a
##data.aws_availability_zones.available.names[1] = us-east-1b



