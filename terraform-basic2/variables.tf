
## VARIABLE BLOCK
variable "ec2_instance" {
    type = string
    description = "instance type"
    default = "t2.micro"
}


variable "ec2_tag_name" {
    type = string
    description = "ec2 tag name"
    default = "ec2_instance"
}


variable "ami_id" {
    type = string
    description = "ami id"
    default = "ami-09d3b3274b6c5d4aa"
}


variable "vpc_cidr" {
    type = string
    description = "vpc cidr"
    default = "10.0.0.0/16"
}


variable "public_subnet_cidr" {
    type = list
    description = "cidr of subnets"
    default = ["10.0.0.0/24", "10.0.2.0/24", "10.0.4.0/24"]
}

variable "private_subnet_cidr" {
    type = list
    description = "cidr of subnets"
    default = ["10.0.1.0/24", "10.0.3.0/24", "10.0.5.0/24"]
}