## Types of modules (Root / Child)
## Classes of modules (Public / private)
## Private module is specific to a company and not accessible to all. Code stored in github or other source control
## other terraform blocks canbe used within the module block
# Modules are dynamic. Meaning these can grow 
# The root module is complete. It contains everything
# Child modules provides value to use the root module creating resources

## (PUBLIC VPC Module)

provider "aws" {
    region = "us-east-1"
    profile = "default"
}

## TERRAFORM BLOCK
terraform {
    required_version = ">=1.1.0"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"    ## AWS API version
    }
  }
}


data "aws_availability_zones" "available" {
  state = "available"
}


module "vpc" {
  source = "terraform-aws-modules/vpc/aws"  #End point (dns name)

  name = "my-vpc"
  cidr = "10.0.0.0/16"

  azs             = slice(data.aws_availability_zones.available.names, 0,3)
  private_subnets = var.private_subnet_cidr
  public_subnets  = var.public_subnet_cidr

  enable_nat_gateway = false
  enable_vpn_gateway = true

  tags = {
    Terraform = "true"
    Environment = "dev"
  }
}