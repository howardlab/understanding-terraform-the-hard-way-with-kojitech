variable "private_subnet_cidr" {
    description = "private subnet Vpc Cidr"
    type = list
    default = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]

}

variable "public_subnet_cidr" {
    description = "public subnet Vpc Cidr"
    type = list
    default = ["10.0.101.0/24", "10.0.102.0/24", "10.0.103.0/24"]

}

