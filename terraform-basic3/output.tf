## Get the vpc id and the ids of the subnets (public and private)
## what ever child needs to output first needs to be outputted in the root module
## check the root module output

output "module_vpc_id" {
    value = module.vpc.vpc_id     ## this will output all the vpc and relates resource attributes. output a dictionary
}

output "module_public_subnets_id" {
    value = module.vpc.public_subnets     ## this will output all the vpc and relates resource attributes. output a dictionary
}

output "module_private_subnets_id" {
    value = module.vpc.private_subnets     ## this will output all the vpc and relates resource attributes. output a dictionary
}