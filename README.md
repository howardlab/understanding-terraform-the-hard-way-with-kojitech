## step-01: Introduction 

- Understand BAsic Terraform Commands 
    - terraform init
    - terraform validate
    - terraform plan
    - terraform apply
    - terraform destroy
    - terraform refresh
    - terraform output

## terraform console
    - dictionary of attributes that can be obtained from any resource
    - [aws_instance.ec2_instance] - can adda dot to get the required attribute

 bash git.command.sh 

## variable data type

'''''''''bash

 " " :> string
 [] = list 
 80 = number 
 bool = true/false 
 {}  =  map 

 ### complicated
 [""] => list(string)
 [{}] => list(map)
 {[]} => map(list)
 Object variable --> combination of all variable types
 """""
