

locals {
    azs = data.aws_availability_zones.available.names #pulls all azs
}



# Create a VPC
resource "aws_vpc" "this" {
  cidr_block = "10.0.0.0/16"
}

## create 2 public subnets, 2 private subnets, 2 database subnets - use one resource bloack for each subnet category
##without meta argument this will be 6 resource blocks.tags
## use count to do this 

resource "aws_subnet" "public_subnet" {
    count = 2

  vpc_id     = aws_vpc.this.id
  cidr_block = var.public_cidr[count.index] #cidr cannot be duplicated. create a variable
  availability_zone = [local.azs[0], local.azs[2]][count.index] #use data source to pull AZs
  map_public_ip_on_launch = true

  tags = {
    Name = "public_subnet_${count.index + 1}"    #can call a variable or a local
  }
}


resource "aws_subnet" "private_subnet" {
    count = 2

  vpc_id     = aws_vpc.this.id
  cidr_block = var.private_cidr[count.index] #cidr cannot be duplicated. create a variable
  availability_zone = [local.azs[0], local.azs[2]][count.index] #use data source to pull AZs
  map_public_ip_on_launch = false

  tags = {
    Name = "private_subnet_${count.index + 1}"    #can call a variable or a local
  }
}


resource "aws_subnet" "database_subnet" {
    count = 2

  vpc_id     = aws_vpc.this.id
  cidr_block = var.database_cidr[count.index] #cidr cannot be duplicated. create a variable
  availability_zone = [local.azs[0], local.azs[2]][count.index] #use data source to pull AZs
  map_public_ip_on_launch = false

  tags = {
    Name = "database_subnet_${count.index + 1}"    #can call a variable or a local
  }
}