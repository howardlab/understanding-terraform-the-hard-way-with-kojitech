## Variable block 

variable "public_cidr"{
    type = list(any)
    description = "public subnet cidr"
    default = ["10.0.0.0/24", "10.0.2.0/24"]
}


variable "private_cidr"{
    type = list(any)
    description = "private subnet cidr"
    default = ["10.0.1.0/24", "10.0.3.0/24"]
}


variable "database_cidr"{
    type = list(any)
    description = "private subnet cidr"
    default = ["10.0.4.0/24", "10.0.6.0/24"]
}