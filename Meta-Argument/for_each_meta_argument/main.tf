

locals {
    azs = data.aws_availability_zones.available.names #pulls all azs

    ##create a map object
    public_subnet = {
        public_subnet_1 = {
            cidr_block = "10.0.1.0/24"
            availability_zone = local.azs[0]
        }

        public_subnet_2 = {
            cidr_block = "10.0.3.0/24"
            availability_zone = local.azs[1]
        }
    }

    private_subnet = {
      private_subnet_1 = {
        cidr_block = "10.0.2.0/24"
        availability_zone = local.azs[0]
      }

      private_subnet_2 = {
        cidr_block = "10.0.4.0/24"
        availability_zone = local.azs[1]
      }
    }


        database_subnet = {
      private_subnet_1 = {
        cidr_block = "10.0.5.0/24"
        availability_zone = local.azs[0]
      }

      private_subnet_2 = {
        cidr_block = "10.0.7.0/24"
        availability_zone = local.azs[1]
      }
    }
}

##### A MAP OF MAP (MAP THAT CONTAINS A MAP)

##MAP OF LIST ##

# Create a VPC
resource "aws_vpc" "this" {
  cidr_block = "10.0.0.0/16"
}

## create 2 public subnets, 2 private subnets, 2 database subnets - use one resource bloack for each subnet category
##without meta argument this will be 6 resource blocks.tags
## use for each to do this
## for each will take maps (accept a key and a value)
## iterate over each of the values 

resource "aws_subnet" "public_subnet" {
    for_each = local.public_subnet  ##iterate over keys in local public_subnet_1 and public_subnet_2

  vpc_id     = aws_vpc.this.id
  cidr_block = each.value.cidr_block ##for each key get this value for cidr_block
  availability_zone = each.value.availability_zone ##for each key get this value for availability_zone
  map_public_ip_on_launch = true

  tags = {
    Name = each.key   #can call a variable or a local
  }
}


resource "aws_subnet" "private_subnet" {
    for_each = local.private_subnet  

  vpc_id     = aws_vpc.this.id
  cidr_block = each.value.cidr_block 
  availability_zone = each.value.availability_zone 
  map_public_ip_on_launch = false

  tags = {
    Name = each.key   
  }
}


resource "aws_subnet" "database_subnet" {
    for_each = local.database_subnet  

  vpc_id     = aws_vpc.this.id
  cidr_block = each.value.cidr_block 
  availability_zone = each.value.availability_zone 

  tags = {
    Name = each.key  
  }
}


