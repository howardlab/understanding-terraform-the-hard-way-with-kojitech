########### variables (types of variables .........) # string
# ' ' # single string
# " " # double string 
## Be descriptive in creating variables 

variable "ami_id" {
    type = string
    description = "ami id"
    default = "ami-09d3b3274b6c5d4aa"
}

variable "ec2_instance" {
    type = string
    description = "instance type"
    default = "t2.micro"
}

variable "cidr_block" {
    type = string
    description = "vpc cidr block"
    default = "10.0.0.0/16"
}

variable "ec2_tag_name" {
    type = string
    description = "ec2 tag name"
    default = "ec2_instance"
}