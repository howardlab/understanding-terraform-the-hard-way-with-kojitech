## How to use data source get pull down an AMIs (amazon machine immage)
## search for ami in terraform docs
## to pull down we need to filter out only what we need. This will help to reduce the amount of data returned 
## pull down subnets that are private only
## if its a private raise ticket and ask for specifications
## can use any number of AMIs but min of 2. be as specific as possible

data "aws_ami" "ami_howard" {
   most_recent      = true ## need to always get the most recent ami as these are always updated
   owners           = ["amazon"]   ##  ["self"] - self refers to private ami. We can use owner or alias for the public or private ami

  filter {
    name   = "name"   #filter ami name. use the value
    values = ["amzn2-ami-kernel-5.10-hvm-*-gp2"] ## amzn2-ami-kernel-5.10-hvm-2.0.20221004.0-x86_64-gp2 = use wild card
  }

  filter {
    name   = "root-device-type" # name of root device type
    values = ["ebs"]
  }

  filter {
    name   = "virtualization-type" #name of visualization type
    values = ["hvm"]
  }
}

## run - terraform apply -target data.aws_ami.ami_howard.id