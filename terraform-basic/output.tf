## instance_id or the ip_address /public or private
# terraform print private ip address

output "private_dns" {
    description = "private dns id"
    value = aws_instance.ec2_instance.private_dns
}