
##this block is meant to set constraint on terraform version 
##################Terraform Block
terraform {
    required_version = ">=1.1.0"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"    ## AWS API version
    }
  }
}

##Provider Block (Authentication and Authorization)
##First approach to access aws
# provider "aws" {
#   region     = "us-east-1"
#   access_key = "my-access-key"
#   secret_key = "my-secret-key"
# }

##second approach use env variables
#provider "aws" {} #Export enviroment variables
##flaw in approach is always run env variable from same path. reconfiguring new terminal if exited

# best to use the assume roles methods
# aws configure --profile profilename

#Using the profiles approach 
provider "aws" {
    region = "us-east-1"
    profile = "default"
}


##Resource Block - Create and manage aws resources - bring into existence
##Cannot use a resource block if the resource already exist - use data sources to pull down available resources in AWS

resource "aws_vpc" "main" {
  cidr_block       = var.cidr_block    # CIDR (re-usable)
  instance_tenancy = "default"

  tags = {
    Name = var.ec2_tag_name
  }
}

##Data Source used to pull down available resources in AWS 

##terraform init - makes an API call to the aws account to validate the user profile. 
#Terraform then downloads keys after successful validation
#Used to validate secret key and access key 

##WHEN TO RUN TERRAFORM INIT
#### - launch/setup of a new project
#### -if change made to code in provider block 

# how to reference a variable (var.variable_name)

# Creating EC2
          #local_name   #resource_name
resource "aws_instance" "ec2_instance" {
  ami           = var.ami_id  # us-esast-1 or use data-source pulled down ami = data.aws_ami.ami_howard.id
  instance_type = var.ec2_instance

  tags = {
    name = var.ec2_tag_name
  }

}

## Creation of a subnet 
##     resource_name   local_name
## depend on flag is created telling resource to wait till resource is created
resource "aws_subnet" "private1" {
  vpc_id     = aws_vpc.main.id   #resource_name.local_name.attributeclear
  cidr_block = "10.0.1.0/24"
  ## parameters ---> from doc -- Reference Argument
  availability_zone = "us-east-1a"    #Data source

}

resource "aws_subnet" "private2" {
  vpc_id     = aws_vpc.main.id   #resource_name.local_name.attributeclear
  cidr_block = "10.0.3.0/24"
  ## parameters ---> from doc -- Reference Argument
  availability_zone = "us-east-1b"

}

resource "aws_subnet" "private3" {
  vpc_id     = aws_vpc.main.id   #resource_name.local_name.attributeclear
  cidr_block = "10.0.5.0/24"
  ## parameters ---> from doc -- Reference Argument
  availability_zone = "us-east-1b"

}

resource "aws_subnet" "private4" {
  vpc_id     = aws_vpc.main.id   #resource_name.local_name.attributeclear
  cidr_block = "10.0.6.0/24"
  ## parameters ---> from doc -- Reference Argument
  availability_zone = "us-east-1a"

}

variable "subnet_zone_cidrs" {
  type    = list(string)
  default = ["10.0.0.0/24", "10.0.0.1/32"]
}


