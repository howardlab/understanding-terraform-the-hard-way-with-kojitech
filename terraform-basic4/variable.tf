

variable "ami_id" {
    type = string
    description = "ami id"
    default = "ami-09d3b3274b6c5d4aa"
}

variable "ec2_instance" {
    type = string
    description = "instance type"
    default = "t2.micro"
}

variable "cidr_block" {
    type = string
    description = "vpc cidr block"
    default = "10.0.0.0/16"
}

variable "ec2_tag_name" {
    type = string
    description = "ec2 tag name"
    default = "ec2_instance"
}

variable "subnet_zone_cidrs" {
  type    = list(string)
  default = ["10.0.0.0/24", "10.0.0.1/32"]
}

variable "public_subnet_cidr" {
    type = list(any)
    description = "cidr of subnets"
    default = ["10.0.0.0/24", "10.0.2.0/24", "10.0.4.0/24", "10.0.6.0/24"]
}

variable "private_subnet_cidr" {
    type = list(any)
    description = "cidr of subnets"
    default = ["10.0.1.0/24", "10.0.3.0/24", "10.0.5.0/24", "10.0.7.0/24"] # number of elements = 4

}
# ## conditional statement 
# variable "create_vpc" {
#     description = "check if a variable exist before creation"
#     type = bool
#     default = true
# }