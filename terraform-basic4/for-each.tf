## create 5 instances using for-each with windows AMI and the last 3 should use an Ubuntu AMI.
## with the concept of for-each try to achieve this
## ["ami-09d3b3274b6c5d4aa", "ami-08c40ec9ead489470"]
## count supports list while for-each supports map object

## resoure block to create 5 instances
resource "aws_instance" "front_end" {


  ami           = ["ami-09d3b3274b6c5d4aa", "ami-08c40ec9ead489470"]
  instance_type = "t3.micro"

  tags = {
    Name = "fronetend"
  }
}

## Create a map object using local
locals {
    frontend_instance = ""
}
