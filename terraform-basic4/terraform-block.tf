############Terraform Block

terraform {
    required_version = ">=1.1.0"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}

#Using the profiles approach 
provider "aws" {
    region = "us-east-1"
    profile = "default"
}

locals {
  local_vpc_id = aws_vpc.main.id
  azs = data.aws_availability_zones.available.names #can also be used for public and database subnets
}

#Resource Block
resource "aws_vpc" "main" {
  cidr_block       = var.cidr_block
  instance_tenancy = "default"

  tags = {
    Name = var.ec2_tag_name
  }
}

# Creating EC2
          #local_name   #resource_name
resource "aws_instance" "ec2_instance" {
  ami           = data.aws_ami.ami_howard.id
  instance_type = var.ec2_instance

  tags = {
    name = var.ec2_tag_name
  }

}

## Creation of a subnet 
##     resource_name   local_name
## depend on flag is created telling resource to wait till resource is created
## need to ensure the data is created with differnt cidrs
## need to ensure this is also created in different regions

resource "aws_subnet" "private1" {
    count = length(var.private_subnet_cidr) # use the length function 

  vpc_id     = aws_vpc.main.id   #resource_name.local_name.attributeclear
  cidr_block = var.private_subnet_cidr[count.index]
  ## parameters ---> from doc -- Reference Argument
  #availability_zone = slice(data.aws_availability_zones.available.names, 0, 2)   #Data source
  #availability_zone = ["us-east-1a", "us-east-1b"][count.index] # to use this number of Azs needs to be same as the number of cidrs. Concept of elements 
  #availability_zone = data.aws_availability_zones.available.names[0] #use the concept of elements to solve this issue
  #availability_zone = slice(local.azs, 0, 2)[count.index]
  availability_zone = element(slice(local.azs, 0, 2), count.index)

  tags = {

    Name = "private_subnet"
  }

}


data "aws_availability_zones" "available" {
  state = "available"
}


output "subnets_ids" {
    description = "private subnet id"
    value = ["${aws_subnet.private1}"]

}

output "private_subnet_without_count"{ #we can output using the splat operator
  value = ""
}

## legacy splat operator
output "private_subnet_with_count" {
  value = aws_subnet.private1.*.id
}

## latest splat operator
output "private_subnet_with_count" {
  value = aws_subnet.private1[*].id ##To get a single ID pass index in place of the wildcard , or slice(aws_subnet_private[*].id, 0, 2)
}


# resource "aws_subnet" "private2" {
#   vpc_id     = aws_vpc.main.id   #resource_name.local_name.attributeclear
#   cidr_block = "10.0.3.0/24"
#   ## parameters ---> from doc -- Reference Argument
#   availability_zone = "us-east-1b"

# }

# resource "aws_subnet" "private3" {
#   vpc_id     = aws_vpc.main.id   #resource_name.local_name.attributeclear
#   cidr_block = "10.0.5.0/24"
#   ## parameters ---> from doc -- Reference Argument
#   availability_zone = "us-east-1b"

# }

# resource "aws_subnet" "private4" {
#   vpc_id     = aws_vpc.main.id   #resource_name.local_name.attributeclear
#   cidr_block = "10.0.6.0/24"
#   ## parameters ---> from doc -- Reference Argument
#   availability_zone = "us-east-1a"

# }

# group the resources into different blocks 
# private and public subnet blocks


#Function
# slice = [1, 2, 3, 4, 5]  ==> specific values returned $[1, 4]
# length = calculate amount of elts in a list (length(var.NameOfVariable)) reveals the amount of elts in a list variable
# [count.index]

## Elemets Explained
## ["10.0.1.0/24", "10.0.3.0/24", "10.0.5.0/24", "10.0.7.0/24",  "10.0.9.0/24"]
## ["us-east-1a", "us-east-1b"]
## ---> Where nothing is specifies start from the beginning again and iterate
## element(slice(local.azs, 0, 2), count.index)

